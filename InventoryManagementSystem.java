import java.util.ArrayList;
import java.util.Scanner;

class Product {
    private String name;
    private double price;
    private int quantity;

    public Product(String name, double price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Name: " + name + ", Price: $" + price + ", Quantity: " + quantity;
    }
}

public class InventoryManagementSystem {
    private static ArrayList<Product> inventory = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("\nInventory Management System");
            System.out.println("1. Add Product");
            System.out.println("2. Remove Product");
            System.out.println("3. View Inventory");
            System.out.println("4. Exit");
            System.out.print("Select an option: ");

            int choice = scanner.nextInt();
            scanner.nextLine();  // Consume the newline character

            switch (choice) {
                case 1:
                    System.out.print("Enter product name: ");
                    String name = scanner.nextLine();
                    System.out.print("Enter product price: $");
                    double price = scanner.nextDouble();
                    System.out.print("Enter product quantity: ");
                    int quantity = scanner.nextInt();
                    Product product = new Product(name, price, quantity);
                    inventory.add(product);
                    System.out.println("Product added to inventory.");
                    break;
                case 2:
                    System.out.print("Enter the name of the product to remove: ");
                    String productToRemove = scanner.nextLine();
                    boolean removed = false;
                    for (Product p : inventory) {
                        if (p.getName().equalsIgnoreCase(productToRemove)) {
                            inventory.remove(p);
                            removed = true;
                            break;
                        }
                    }
                    if (removed) {
                        System.out.println("Product removed from inventory.");
                    } else {
                        System.out.println("Product not found in inventory.");
                    }
                    break;
                case 3:
                    System.out.println("\nInventory:");
                    for (Product p : inventory) {
                        System.out.println(p.toString());
                    }
                    break;
                case 4:
                    System.out.println("Exiting the application.");
                    System.exit(0);
                default:
                    System.out.println("Invalid choice. Please select a valid option.");
                    break;
            }
        }
    }
}
